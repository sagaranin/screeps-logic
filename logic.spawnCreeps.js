var spawnCreeps = {

    /** @param {Creep} creep **/
    spawn: function() {
        
        var spawnName = "Spawn1";
        var spawn = Game.spawns['Spawn1'];
        
        var h_max = 6;
        var u_max = 8;
        var b_max = 2;
        
        var h_count = 0;
        var u_count = 0;
        var b_count = 0;
        
        h_parts = [WORK, WORK, WORK, CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE];
        u_parts = [WORK, WORK, WORK, CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE];
        b_parts = [WORK, WORK, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE];
        
        // 
        for(var name in Game.creeps) {
            var creep = Game.creeps[name];
            
            if(creep.memory.role == 'harvester') {
                h_count++;
            }
            if(creep.memory.role == 'upgrader') {
                u_count++;
            }
            if(creep.memory.role == 'builder') {
                b_count++;
            }
        }
        
        if (h_count < h_max) {
            if (spawn.spawning == null && spawn.energy == spawn.energyCapacity && spawnCreeps.testSpawn(h_parts)){
                spawn.spawnCreep(h_parts, 'h'+Math.round(Math.random()*1000), { memory: { role: 'harvester' } } );
                console.log("h: "+h_count+", u: "+u_count + ", b: " + b_count);
                console.log("Spawning new Harvester");
            
                return;
            }
        }
        
        if (u_count < u_max) {
            if (spawn.spawning == null && Game.spawns['Spawn1'].energy == spawn.energyCapacity && spawnCreeps.testSpawn(u_parts)){
                spawn.spawnCreep(u_parts, 'u'+Math.round(Math.random()*1000), { memory: { role: 'upgrader' } } );
                console.log("h: "+h_count+", u: "+u_count + ", b: " + b_count);
                console.log("Spawning new Upgrader");
                
                return;
            }
        }
        
        if (b_count < b_max) {
            if (spawn.spawning == null && spawn.energy == spawn.energyCapacity && spawnCreeps.testSpawn(b_parts)){
                spawn.spawnCreep(b_parts, 'b'+Math.round(Math.random()*1000), { memory: { role: 'builder' } } );
                console.log("h: "+h_count+", u: "+u_count + ", b: " + b_count);
                console.log("Spawning new Builder");
                
                return;
            }
        }

	},
	
	testSpawn: function(parts){
	    return Game.spawns['Spawn1'].spawnCreep(parts, 'Test', { dryRun: true }) == 0;
	}
};

module.exports = spawnCreeps;