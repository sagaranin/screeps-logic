var roleUpgrader = {

    /** @param {Creep} creep **/
    run: function(creep) {
        // если трюм полный - переключаем флаг в режим апгрейдера
        if(creep.carry.energy == creep.carryCapacity && creep.memory.flag !== "DO-UPGRADE") {
            creep.memory.flag = "DO-UPGRADE";
            creep.say("Go Upgrade!");
        // если трюм пуст - переключаем флаг в режим добычи
        } else if (creep.carry.energy == 0 && creep.memory.flag !== "DO-HARVEST"){
            creep.memory.flag = "DO-HARVEST";
             creep.say("Go Harvest!");
        } 
        
        
	    if(creep.memory.flag === "DO-HARVEST") {
            var source = creep.pos.findClosestByPath(FIND_SOURCES);
            if(creep.harvest(source) == ERR_NOT_IN_RANGE) {
                creep.moveTo(source, {visualizePathStyle: {stroke: '#00b300'}});
            }
        }
        else if (creep.memory.flag === "DO-UPGRADE") {
            if(creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.controller, {visualizePathStyle: {stroke: '#00b300'}});
            }
        }
	}
};

module.exports = roleUpgrader;