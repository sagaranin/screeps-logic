//roles
var roleHarvester = require('role.harvesterv2');
var roleUpgrader = require('role.upgrader');
var roleBuilder = require('role.builder');

// other
var spawnCreeps = require('logic.spawnCreeps');

module.exports.loop = function () {
    
    spawnCreeps.spawn();

    for(var name in Game.creeps) {
        var creep = Game.creeps[name];
        if(creep.memory.role == 'harvester') {
            roleHarvester.run(creep);
        }
        if(creep.memory.role == 'upgrader') {
            roleUpgrader.run(creep);
        }
        if(creep.memory.role == 'builder') {
            roleBuilder.run(creep);
        }
    }
}
