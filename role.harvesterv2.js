var roleHarvesterv2 = {

    run: function(creep) {
        //если флаг не установлен - ставим по дефолту
        if (creep.memory.flag == null) {creep.memory.flag = "DO-HARVEST"}
        
        // если трюм полный - переключаем флаг в режим апгрейдера
        if(creep.carry.energy == creep.carryCapacity && creep.memory.flag !== "DO-FILL") {
            creep.memory.flag = "DO-FILL";
            creep.say("Go Fill!");
        // если трюм пуст - переключаем флаг в режим добычи
        } else if (creep.carry.energy == 0 && creep.memory.flag !== "DO-HARVEST"){
            creep.memory.flag = "DO-HARVEST";
             creep.say("Go Harvest!");
        } 
        
        if(creep.memory.flag === "DO-HARVEST") {
            roleHarvesterv2.harvestResources(creep);
        }
        else if (creep.memory.flag === "DO-FILL") {
            if (Game.spawns['Spawn1'].energy < Game.spawns['Spawn1'].energyCapacity) {
                if(creep.transfer(Game.spawns['Spawn1'], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                 creep.moveTo(Game.spawns['Spawn1'], {visualizePathStyle: {stroke: '#ffff4d'}});
                }
            } else {
                roleHarvesterv2.fillStructures(creep);
            }
        }
	},
		
	fillStructures: function(creep) {
        var targets = creep.room.find(FIND_STRUCTURES, {
            filter: (structure) => {
                return (((structure.structureType == STRUCTURE_EXTENSION || structure.structureType == STRUCTURE_TOWER)) && structure.energy < structure.energyCapacity) 
                    || (structure.structureType == STRUCTURE_CONTAINER && _.sum(structure.store) < structure.storeCapacity )}
            });
            
        targets.sort(function(a,b){return a.energy < b.energy ? -1 : 1});
        if (targets.length > 0) {
            if (creep.transfer(targets[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(targets[0], { visualizePathStyle: { stroke: '#ffff4d' }});
            }
        } else {
            creep.moveTo(Game.spawns['Spawn1']);
        }
	}, 
	
    harvestResources: function(creep) {
        var source = creep.pos.findClosestByPath(FIND_SOURCES);
        
        if(creep.harvest(source) == ERR_NOT_IN_RANGE) {
            creep.moveTo(source, {visualizePathStyle: {stroke: '#ffff4d'}});
        }
        
        if (source == null && creep.carry.energy != 0) {
            roleHarvesterv2.fillStructures(creep);  
        }  
	}
};

module.exports = roleHarvesterv2;